# WHAT'S THAT?

Firstly, *dvdrec* is just a shell script.

Secondly, *dvdrec* is very simple front-end for the
[growisofs](http://fy.chalmers.se/~appro/linux/DVD+RW/)
tool.

# HOW DOES IT WORK?

It asks you point a directory. Then it writes the files
and subdirectories of the directory you point to DVD.
It also checks if the total size of files in the directory
less or equal to 4.4G. If this condition is wrong, *dvdrec*
will show an error box. Otherwise it will burn a DVD.

Nothing more.

# DEPENDENCIES?

*dvdrec* is a project on early development stage. Because
of this it has got dependencies (and
[issues](https://gitlab.com/vasilyb/dvdrec/issues), of course).
All the dependencies listed below.

1. [zenity](http://www.freebsdsoftware.org/x11/zenity.html)
1. [mate](http://mate-desktop.org/)

# HOW TO USE?

Download the script and run it. Choose a directory you want do
burn to a DVD. Insert a blank DVD into the drive. Click *OK*.